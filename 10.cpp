#include <iostream>
#include <string>
using namespace std;

void inverte(string& s) {
	for (int i = 0; i < s.size() / 2; i++) {
		char temp = ' ';
		temp = s[(s.size() - 1) - i];
		s[(s.size() - 1) - i] = s[i];
		s[i] = temp;
	}
}

int main() {
	string s;
	cout << "Digite a frase: "; cin >> s;
	inverte(s);
	cout << s;
}
