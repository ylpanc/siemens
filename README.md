# Siemens

07.cpp
O programa calcula o termo informado pelo usuário da série de Fibonacci.
O termo informado será passado para uma função fib como argumento e será calculado por meio de recursividade. 

Procedimentos:
  -> Primeiramente Compile o arquivo;
  -> Execute;
  -> Digite o número do termo da série de Fibonacci;
  -> O resultado será fornecido.

	
08.cpp
O programa realiza o cálculo do tamanho da string fornecida pelo usuário sem usar nenhuma função externa.

Procedimentos:
  -> Compile o arquivo;
  -> Execute;
  -> Digite sua string a ser calculada;
  -> O tamanho será fornecido na tela.


09.cpp
O programa fornece o índice da chave escolhida pelo usuário em um vetor de tamanho e valores também escolhidos pelo usuário.

Procedimentos:
  -> Compile o arquivo;
  -> Execute;
  -> Insira o tamanho desejado do vetor;
  -> Insira a chave (elemento a ser buscado);
  -> Insira os valores do vetor;
  -> O índice da chave será apresentado na tela.


10.cpp
O programa realiza a inversão da string fornecida pelo usuário sem a utilização de um segundo buffer e sem utilizar funções externas.

Procedimentos:
  -> Compile o arquivo;
  -> Execute;
  -> Digite a string;
  -> A string invertida será apresentada na tela.
