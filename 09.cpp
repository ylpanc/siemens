#include <iostream>
#include <vector>
using namespace std;

int pesquisa_linear(vector<int> v, int t, int c) {
	for (int i = 0; i < t; i++) {
		if (v[i] == c)
			return i;
	}
	return -1;
}


int main() {

	int t;
	cout << "Insira o tamanho do vetor: "; cin >> t;
	
	vector<int> v(t);

	int c;
	cout << "Insira a chave: "; cin >> c;

	for (int i = 0; i < t; i++)
		cin >> v[i];

	int y = pesquisa_linear(v, t, c);
	cout << endl << "Indice: " << y;
}
